﻿using Core.Enums;
using Core.Models.ServiceOffers;
using FluentValidation;

namespace Service.Validators
{
    public class ServiceOfferValidatorService : AbstractValidator<ServiceOffer>
    {
        public ServiceOfferValidatorService()
        {
            RuleFor(serviceOffer => serviceOffer.Name)
                .NotEmpty()
                .NotNull()
                .MaximumLength(50);

            RuleFor(serviceOffer => serviceOffer.Description)
               .NotEmpty()
               .NotNull()
               .MaximumLength(1000);

        }
    }

}
