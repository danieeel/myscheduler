﻿using Core.Enums;
using Core.Models.Users;
using FluentValidation;

namespace Service.Validators
{
    public class UserValidatorService : AbstractValidator<User>
    {
        public UserValidatorService(ActionTypesEnum actionType)
        {
            switch (actionType)
            {
                case ActionTypesEnum.ADD:
                    CheckName();
                    CheckEmail();
                    CheckProfile();
                    break;
                case ActionTypesEnum.EDIT:
                    CheckName();
                    CheckEmail();
                    CheckProfile();
                    break;
                case ActionTypesEnum.DELETE:
                    break;
               
                default:
                    break;
            }
        }

        private void CheckName()
        {
            RuleFor(user => user.Name)
                .NotEmpty()
                .NotNull()
                .MaximumLength(50);
        }

        private void CheckEmail()
        {
            RuleFor(user => user.Email)
                .NotEmpty()
                .NotNull()
                .MaximumLength(200)
                .EmailAddress();
        }

        private void CheckProfile()
        {
            RuleFor(user => user.Profile)
                .IsInEnum()
                .NotNull()
                .NotEmpty()
                .WithMessage("Invalid User Profile.");
               
        }
    }
}
