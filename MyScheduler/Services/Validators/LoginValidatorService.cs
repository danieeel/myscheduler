﻿using Core.Filters.Login;
using FluentValidation;

namespace Service.Validators
{
    public class LoginValidatorService : AbstractValidator<UserLoginFilter>
    {
        public LoginValidatorService()
        {
            RuleFor(userLogin => userLogin.Email)
                .NotEmpty()
                .NotNull()
                .MaximumLength(200)
                .EmailAddress();

            RuleFor(userLogin => userLogin.Password)
                .NotEmpty()
                .NotNull()
                .MaximumLength(200);

            RuleFor(userLogin => userLogin.Profile)
               .IsInEnum()
               .NotNull()
               .NotEmpty()
               .WithMessage("Invalid User Profile.");
        }    
    }
}
