﻿using Core;
using Core.Enums;
using Core.Exceptions;
using Core.Filters.ServiceOffers;
using Core.Models.ServiceOffers;
using Core.Services;
using FluentValidation.Results;
using Service.Validators;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class ServiceOfferService : IServiceOfferService
    {
        #region PROPERTIES

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region CONSTRUCTORS
        public ServiceOfferService()
        {

        }
        public ServiceOfferService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        #endregion

        #region PUBLIC METHODS
        public async Task<ServiceOffer> Add(ServiceOffer serviceOffer)
        {
            var validationResult = await ValidateServiceOffer(serviceOffer).ConfigureAwait(true);

            if (!validationResult.IsValid)
                throw new BusinessException(validationResult.Errors.Select(x => x.ErrorMessage).LastOrDefault());

            await _unitOfWork.ServiceOffers.AddAsync(serviceOffer).ConfigureAwait(true);
            await _unitOfWork.CommitAsync().ConfigureAwait(true);

            return serviceOffer;
        }

        public async Task Delete(ServiceOffer serviceOffer)
        {
            _unitOfWork.ServiceOffers.Remove(serviceOffer);
            await _unitOfWork.CommitAsync().ConfigureAwait(true);
        }

        public IEnumerable<ServiceOffer> ApplyFilter(ServiceOfferListFilter filter)
        {
            return _unitOfWork.ServiceOffers.ApplyFilter(filter);
        }

        public IEnumerable<ServiceOffer> ApplySort(ServiceOfferListSortFilter sortFilter, IEnumerable<ServiceOffer> serviceOffers)
        {
            return _unitOfWork.ServiceOffers.ApplySort(sortFilter, serviceOffers);
        }

        public IEnumerable<ServiceOffer> ApplyPagination(ServiceOfferListPaginatedFilter paginationFilter, IEnumerable<ServiceOffer> serviceOffers)
        {
            return _unitOfWork.ServiceOffers.ApplyPagination(paginationFilter, serviceOffers);
        }

        public async Task<IEnumerable<ServiceOffer>> GetAll()
        {
            return await _unitOfWork.ServiceOffers.GetAllAsync().ConfigureAwait(true);
        }

        public async Task<ServiceOffer> GetById(int id)
        {
            return await _unitOfWork.ServiceOffers.GetByIdAsync(id).ConfigureAwait(true);
        }

        public async Task Update(ServiceOffer serviceOfferToBeUpdated, ServiceOffer serviceOffer)
        {
            var validationResult = await ValidateServiceOffer(serviceOffer).ConfigureAwait(true);

            if (!validationResult.IsValid)
                throw new BusinessException(validationResult.Errors.Select(x => x.ErrorMessage).LastOrDefault());

            if (serviceOfferToBeUpdated != null && serviceOffer != null)
            {
                serviceOfferToBeUpdated.Name = serviceOffer.Name;
                serviceOfferToBeUpdated.Description = serviceOffer.Description;
                serviceOfferToBeUpdated.IsActive = serviceOffer.IsActive;

            }

            await _unitOfWork.CommitAsync().ConfigureAwait(true);
        }

        #endregion

        #region PRIVATE METHODS
        private async Task<ValidationResult> ValidateServiceOffer(ServiceOffer serviceOffer)
        {
            var validator = new ServiceOfferValidatorService();
            var result = await validator.ValidateAsync(serviceOffer).ConfigureAwait(true);

            return result;
        }



        #endregion

    }
}
