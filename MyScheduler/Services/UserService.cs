﻿using Core;
using Core.Enums;
using Core.Exceptions;
using Core.Filters.Users;
using Core.Models.Users;
using Core.Services;
using FluentValidation.Results;
using Service.Validators;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class UserService : IUserService
    {
        #region PROPERTIES

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region CONSTRUCTORS
        public UserService()
        {

        }
        public UserService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        #endregion

        #region PUBLIC METHODS
        public async Task<User> Add(User user)
        {
            var validationResult = await ValidateUserAction(user, ActionTypesEnum.ADD).ConfigureAwait(true);

            if (!validationResult.IsValid)
                throw new BusinessException(validationResult.Errors.Select(x => x.ErrorMessage).LastOrDefault());

            await _unitOfWork.Users.AddAsync(user).ConfigureAwait(true);
            await _unitOfWork.CommitAsync().ConfigureAwait(true);

            return user;
        }

        public async Task Delete(User user)
        {
            _unitOfWork.Users.Remove(user);
            await _unitOfWork.CommitAsync().ConfigureAwait(true);
        }

        public IEnumerable<User> ApplyFilter(UserListFilter filter)
        {
            return _unitOfWork.Users.ApplyFilter(filter);
        }

        public IEnumerable<User> ApplySort(UserListSortFilter sortFilter, IEnumerable<User> users)
        {
            return _unitOfWork.Users.ApplySort(sortFilter, users);
        }

        public IEnumerable<User> ApplyPagination(UserListPaginatedFilter paginationFilter, IEnumerable<User> users)
        {
            return _unitOfWork.Users.ApplyPagination(paginationFilter, users);
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await _unitOfWork.Users.GetAllAsync().ConfigureAwait(true);
        }

        public async Task<User> GetById(int id)
        {
            return await _unitOfWork.Users.GetByIdAsync(id).ConfigureAwait(true);
        }

        public async Task Update(User userToBeUpdated, User user)
        {
            var validationResult = await ValidateUserAction(user, ActionTypesEnum.EDIT).ConfigureAwait(true);

            if (!validationResult.IsValid)
                throw new BusinessException(validationResult.Errors.Select(x => x.ErrorMessage).LastOrDefault());

            if (userToBeUpdated != null && user != null)
            {
                userToBeUpdated.Name = user.Name;

            }

            await _unitOfWork.CommitAsync().ConfigureAwait(true);
        }

        #endregion

        #region PRIVATE METHODS
        private async Task<ValidationResult> ValidateUserAction(User user, ActionTypesEnum actionType)
        {
            var validator = new UserValidatorService(actionType);
            var result = await validator.ValidateAsync(user).ConfigureAwait(true);

            return result;
        }



        #endregion

    }
}
