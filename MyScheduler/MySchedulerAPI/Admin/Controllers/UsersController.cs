﻿using AutoMapper;
using Core.Exceptions;
using Core.Filters.Users;
using Core.Models.Users.DTOs;
using Core.Models.Users;
using Core.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MySchedulerAPI.Admin.Controllers
{
    [Route("admin/v0/[controller]")]
    [ApiController]
    //[Authorize(Roles = "Administrator")]
    public class UsersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public UsersController(IUserService userService, IMapper mapper)
        {
            this._mapper = mapper;
            this._userService = userService;
        }

        [HttpGet("Get")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<UserListPaginatedOutputDTO> Get([FromQuery] UserListFilter filter,
                                                [FromQuery] UserListSortFilter sortFilter,
                                                [FromQuery] UserListPaginatedFilter paginationFilter)
        {
            var outputDTO = new UserListPaginatedOutputDTO();
            try
            {
                IEnumerable<User> users = _userService.ApplyFilter(filter);

                users = _userService.ApplySort(sortFilter, users);
                var totalCount = users.Count();

                users = _userService.ApplyPagination(paginationFilter, users);

                if (users.Any())
                {
                    var usersOutputDTO = _mapper.Map<IEnumerable<User>, IEnumerable<UserOutputDTO>>(users);
                    outputDTO = usersOutputDTO.BuildPaginatedResponse(paginationFilter, totalCount);
                }

                return Ok(outputDTO);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<User>> GetById(int id)
        {
            try
            {
                var user = await _userService.GetById(id).ConfigureAwait(true);

                if (user == null) { return NotFound($"User (ID: {id}) not found."); };

                var outputDTO = _mapper.Map<User, UserOutputDTO>(user);

                return Ok(outputDTO);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("Add")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<UserOutputDTO>> Add(UserInputDTO userInputDTO)
        {
            try
            {
                var userToBeCreated = _mapper.Map<UserInputDTO, User>(userInputDTO);
                var newUser = await _userService.Add(userToBeCreated).ConfigureAwait(true);
                var user = await _userService.GetById(newUser.Id).ConfigureAwait(true);
                var outputDTO = _mapper.Map<User, UserOutputDTO>(user);

                return Ok(outputDTO);
            }
            catch (BusinessException ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPut("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<User>> Update(int id, [FromBody] UserInputDTO userInputDTO)
        {
            try
            {
                var userToBeUpdated = await _userService.GetById(id).ConfigureAwait(true);

                if (userToBeUpdated == null) { return NotFound($"User (ID: {id}) not found."); };

                var user = _mapper.Map<UserInputDTO, User>(userInputDTO);

                await _userService.Update(userToBeUpdated, user).ConfigureAwait(true);

                return Ok(user);
            }
            catch (BusinessException ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpDelete("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var user = await _userService.GetById(id).ConfigureAwait(true);

                if (user == null) { return NotFound($"User (ID: {id}) not found."); };

                await _userService.Delete(user).ConfigureAwait(true);

                return Ok($"User (ID: {id}) deleted.");
            }
            catch (BusinessException ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
