﻿using AutoMapper;
using Core.Exceptions;
using Core.Filters.ServiceOffers;
using Core.Models.ServiceOffers.DTOs;
using Core.Models.ServiceOffers;
using Core.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MySchedulerAPI.Admin.Controllers
{
    [Route("admin/v0/[controller]")]
    [ApiController]
    //[Authorize(Roles = "Administrator")]
    public class ServiceOffersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IServiceOfferService _serviceOfferService;

        public ServiceOffersController(IServiceOfferService serviceOfferservice, IMapper mapper)
        {
            this._mapper = mapper;
            this._serviceOfferService = serviceOfferservice;
        }

        [HttpGet("Get")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<ServiceOfferListPaginatedOutputDTO> Get([FromQuery] ServiceOfferListFilter filter,
                                                [FromQuery] ServiceOfferListSortFilter sortFilter,
                                                [FromQuery] ServiceOfferListPaginatedFilter paginationFilter)
        {
            var outputDTO = new ServiceOfferListPaginatedOutputDTO();
            try
            {
                IEnumerable<ServiceOffer> serviceOffers = _serviceOfferService.ApplyFilter(filter);

                serviceOffers = _serviceOfferService.ApplySort(sortFilter, serviceOffers);
                var totalCount = serviceOffers.Count();

                serviceOffers = _serviceOfferService.ApplyPagination(paginationFilter, serviceOffers);

                if (serviceOffers.Any())
                {
                    var serviceOffersOutputDTO = _mapper.Map<IEnumerable<ServiceOffer>, IEnumerable<ServiceOfferOutputDTO>>(serviceOffers);
                    outputDTO = serviceOffersOutputDTO.BuildPaginatedResponse(paginationFilter, totalCount);
                }

                return Ok(outputDTO);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ServiceOffer>> GetById(int id)
        {
            try
            {
                var serviceOffer = await _serviceOfferService.GetById(id).ConfigureAwait(true);

                if (serviceOffer == null) { return NotFound($"Service Offer (ID: {id}) not found."); };

                var outputDTO = _mapper.Map<ServiceOffer, ServiceOfferOutputDTO>(serviceOffer);

                return Ok(outputDTO);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("Add")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<ServiceOfferOutputDTO>> Add([FromBody] ServiceOfferInputDTO serviceOfferInputDTO)
        {
            try
            {
                var serviceOfferToBeCreated = _mapper.Map<ServiceOfferInputDTO, ServiceOffer>(serviceOfferInputDTO);
                var newServiceOffer = await _serviceOfferService.Add(serviceOfferToBeCreated).ConfigureAwait(true);
                var serviceOffer = await _serviceOfferService.GetById(newServiceOffer.Id).ConfigureAwait(true);
                var outputDTO = _mapper.Map<ServiceOffer, ServiceOfferOutputDTO>(serviceOffer);

                return Ok(outputDTO);
            }
            catch (BusinessException ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPut("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ServiceOffer>> Update(int id, [FromBody] ServiceOfferInputDTO serviceOfferInputDTO)
        {
            try
            {
                var serviceOfferToBeUpdated = await _serviceOfferService.GetById(id).ConfigureAwait(true);

                if (serviceOfferToBeUpdated == null) { return NotFound($"Service Offer (ID: {id}) not found."); };

                var serviceOffer = _mapper.Map<ServiceOfferInputDTO, ServiceOffer>(serviceOfferInputDTO);

                await _serviceOfferService.Update(serviceOfferToBeUpdated, serviceOffer).ConfigureAwait(true);

                return Ok(serviceOffer);
            }
            catch (BusinessException ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpDelete("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var serviceOffer = await _serviceOfferService.GetById(id).ConfigureAwait(true);

                if (serviceOffer == null) { return NotFound($"Service Offer (ID: {id}) not found."); };

                await _serviceOfferService.Delete(serviceOffer).ConfigureAwait(true);

                return Ok($"Service Offer (ID: {id}) deleted.");
            }
            catch (BusinessException ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
