﻿using AutoMapper;
using Core.Enums;
using Core.Models.ServiceOffers;
using Core.Models.ServiceOffers.DTOs;
using Core.Models.Users;
using Core.Models.Users.DTOs;
using System;

namespace MySchedulerAPI.Mapping
{
    public class MappingProfileAPI : Profile
    {
        public MappingProfileAPI()
        {
            CreateMap<User, UserOutputDTO>()
                .ForMember(x => x.Profile,
                 opt => opt.MapFrom(source => Enum.GetName(typeof(UserProfilesEnum), source.Profile))); ; ;

            //// Domain to Resource
            //CreateMap<User, UserOutputDTO>();

            //// Resource to Domain
            //CreateMap<UserInputDTO, User>();

            CreateMap<UserInputDTO, User>()
                .ForMember(x => x.Profile,
                 opt => opt.MapFrom(source => Enum.GetName(typeof(UserProfilesEnum), source.Profile)));


            CreateMap<ServiceOffer, ServiceOfferOutputDTO>();
            CreateMap<ServiceOfferInputDTO, ServiceOffer>();






        }
    }
}
