﻿using Core.Models.Users.DTOs;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebAdmin.Models;
using WebAdmin.Services.Http;

namespace WebAdmin.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserHttpService _httpClient;

        public UserController(IUserHttpService httpClient)
        {
            _httpClient = httpClient;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> UsersAsync(UserListViewModel model)
        {
            if (!ModelState.IsValid) return View();
            
            var result = await _httpClient.GetAll();
            
            if (model.Success.HasValue)
            {
                result.Message = model.Message;
                result.Success = model.Success.Value;
                result.MessageTitle = model.MessageTitle;
            }


            return View(result);
        }

        public IActionResult AddUser()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddUser(AddUserViewModel model)
        {
            if (!ModelState.IsValid)
            {
                model.Success = false;
            }
            else
            {
                try
                {
                    var result = await _httpClient.Add(model);
                    
                    if (result != null)
                    {                   
                        model.Success = true;
                        model.MessageTitle = $"New {result.Profile} created!";
                        model.Message = "User created with success!";
                    }
                    else
                    {
                        ModelState.AddModelError("error", $"StatusCode:");
                        model.Success = false;
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("exception", ex.Message);
                    model.Success = false;
                }
            }
            
            return View(model);
        }

        public IActionResult Delete(int id)
        {
            var model = new UserListViewModel();

            try
            {
                _httpClient.Delete(id);

                model.Success = true;
                model.MessageTitle = $"User deleted!";
                model.Message = "User deleted with success!";

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("exception", ex.Message);
                model.Success = false;
            }

            return RedirectToAction("Users", "User", model);
        }
    }
}