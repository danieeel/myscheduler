﻿using System.Collections.Generic;

namespace WebAdmin.Models
{
    public class UserListViewModel
    {
        public string Search { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public bool HasPrevious { get; set; }
        public bool HasNext { get; set; }
        public bool? Success { get; set; }
        public string MessageTitle { get; set; }
        public string Message { get; set; }

        public IEnumerable<UserViewModel> Items { get; set; }

        public UserListViewModel()
        {
            Items = new List<UserViewModel>();
        }

    }

}
