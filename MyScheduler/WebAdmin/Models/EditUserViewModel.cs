﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAdmin.Models
{
    public class EditUserViewModel
    {
        [Required(ErrorMessage = "Mandatory field")]
        [StringLength(50)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Mandatory field")]
        [StringLength(200)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Mandatory field")]
        [StringLength(200)]
        public String Password { get; set; }
        public bool IsActive { get; set; }

        [Required(ErrorMessage = "Mandatory field")]
        [Display(Name = "Profile")]
        public int Profile { get; set; }

        public bool? Success { get; set; }

        public string MessageTitle { get; set; }
        public string Message { get; set; }

    }
}
