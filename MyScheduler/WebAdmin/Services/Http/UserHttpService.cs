﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebAdmin.Models;

namespace WebAdmin.Services.Http
{
    public class UserHttpService : IUserHttpService
    {
        private const string BASE_API_URL = "http://localhost:62293/admin/v0/Users";
        private readonly IHttpClientFactory _httpClient;
        public UserHttpService(IHttpClientFactory httpClient)
        {
            this._httpClient = httpClient;
        }

        public async Task<UserViewModel> Add(AddUserViewModel model)
        {
            using (var client = _httpClient.CreateClient())
            {
                client.BaseAddress = new Uri($"{BASE_API_URL}/Add");

                using (var request = new HttpRequestMessage(HttpMethod.Post, client.BaseAddress))
                {
                    var json = JsonConvert.SerializeObject(model);
                    request.Content = new StringContent(json, Encoding.UTF8, "application/json");

                    var result = await client
                        .SendAsync(request, HttpCompletionOption.ResponseHeadersRead, new System.Threading.CancellationToken())
                        .ConfigureAwait(true);

                    return await result
                        .Content
                        .ReadAsAsync<UserViewModel>();
                };
            }
        }

        public async void Delete(int id)
        {
            using (var client = _httpClient.CreateClient())
            {
                client.BaseAddress = new Uri($"{BASE_API_URL}/{id}");

                await client.DeleteAsync("");
            
                
            }
        }

        public async Task<UserListViewModel> GetAll()
        {
            using (var client = _httpClient.CreateClient())
            {
                client.BaseAddress = new Uri($"{BASE_API_URL}/Get");

                var result = await client
                    .GetAsync("Get")
                    .ConfigureAwait(true);

                return await result
                    .Content
                    .ReadAsAsync<UserListViewModel>();
            }
        }

        public Task<UserViewModel> GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Task<UserViewModel> Update(UserViewModel user)
        {
            throw new NotImplementedException();
        }
    }
}
