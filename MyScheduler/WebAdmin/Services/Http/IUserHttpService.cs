﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAdmin.Models;

namespace WebAdmin.Services.Http
{
    public interface IUserHttpService
    {
        Task<UserListViewModel> GetAll();
        Task<UserViewModel> GetById(int id);
        Task<UserViewModel> Add(AddUserViewModel user);
        Task<UserViewModel> Update(UserViewModel user);
        void Delete(int id);
    }
}
