using Core.Models.Users;
using Core.Repositories;
using Moq;
using Services;
using System;
using Xunit;

namespace ServicesTests
{
    public class UserServiceTest
    {
        private readonly UserService _userService;
    
        public UserServiceTest()
        {
            _userService = new UserService();
        }

        [Fact]
        public async void ShouldAddUserWithSuccess()
        { 
            var executedAt = DateTime.Now;
            
            var user = new User
            {
                Email = "email@xunit.com",
                CreatedAt = executedAt,
                Name = "xunit user",
                LastUpdate = executedAt,
                IsActive = true
            };

            var userRepositoryMock = new Mock<IUserRepository>();
            userRepositoryMock.Setup(x => x.AddAsync(user));
            
            await userRepositoryMock.Object.AddAsync(user).ConfigureAwait(true);

            userRepositoryMock.Verify(x => x.AddAsync(user), Times.Once());

        }

    }
}
