﻿using Core.Enums;
using Core.Models.Users;
using Service.Validators;
using System;
using System.Linq;
using Xunit;

namespace ServicesTests.Validators
{
    public class UserInputValidatorsTest
    {
        private readonly UserValidatorService _userValidatorServiceADD, _userValidatorServiceEDIT;
        private readonly User _validUser;
        public UserInputValidatorsTest()
        {
            _userValidatorServiceADD = new UserValidatorService(ActionTypesEnum.ADD);
            _userValidatorServiceEDIT = new UserValidatorService(ActionTypesEnum.EDIT);
            _validUser = BuildUser();
        }

        [Theory]
        [InlineData(null, "xunit@email.com")]
        [InlineData(" ", "xunit@email.com")]
        [InlineData("xUnit user", null)]
        [InlineData("xUnit user", " ")]
        public async void AddUserWithoutMandatoryFieldsShouldBeInvalid(string name, string email)
        {
            var user = BuildUser(name, email);
            var result = await _userValidatorServiceADD.ValidateAsync(user).ConfigureAwait(true);

            Assert.NotNull(result);
            Assert.False(result.IsValid, result.Errors.Select(error => error.ErrorMessage).LastOrDefault());
        }

        [Theory]
        [InlineData(null, "xunit@email.com")]
        [InlineData(" ", "xunit@email.com")]
        [InlineData("xUnit user", null)]
        [InlineData("xUnit user", " ")]
        public async void EditUserWithoutMandatoryFieldsShouldBeInvalid(string name, string email)
        {
            var user = BuildUser(name, email);
            var result = await _userValidatorServiceEDIT.ValidateAsync(user).ConfigureAwait(true);

            Assert.NotNull(result);
            Assert.False(result.IsValid, result.Errors.Select(error => error.ErrorMessage).LastOrDefault());
        }

        [Fact]
        public async void AddUserFullFilledShouldBeValid()
        {
            var validator = new UserValidatorService(ActionTypesEnum.ADD);
            var result = await validator.ValidateAsync(_validUser).ConfigureAwait(true);

            Assert.NotNull(result);
            Assert.True(result.IsValid);
        }

        [Fact]
        public async void EditUserFullFilledShouldBeValid()
        {
            var validator = new UserValidatorService(ActionTypesEnum.ADD);
            var result = await validator.ValidateAsync(_validUser).ConfigureAwait(true);

            Assert.NotNull(result);
            Assert.True(result.IsValid);
        }

        private User BuildUser(string email = "xunit@email.com", string name = "xUnit User",
            UserProfilesEnum profile = UserProfilesEnum.Client)
        {
            return new User
            {
                Email = email,
                Name = name,
                Profile = profile,
                CreatedAt = DateTime.Now,
                LastUpdate = DateTime.Now,
                IsActive = true
            };
        }
    }
}
