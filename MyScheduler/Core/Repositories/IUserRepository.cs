﻿using Core.Filters.Users;
using Core.Models.Users;
using System.Collections.Generic;
using System.Linq;

namespace Core.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        IQueryable<User> ApplyFilter(UserListFilter filter);
        IQueryable<User> ApplySort(UserListSortFilter sortFilter, IEnumerable<User> users);
        IQueryable<User> ApplyPagination(UserListPaginatedFilter paginationFilter, IEnumerable<User> users);
    }
}
