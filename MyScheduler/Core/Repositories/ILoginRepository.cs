﻿using Core.Filters.Login;
using Core.Models.Users;

namespace Core.Repositories
{
    public interface ILoginRepository : IRepository<User>
    {
        User Login(UserLoginFilter filter);
       
    }
}
