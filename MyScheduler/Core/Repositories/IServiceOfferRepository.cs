﻿using Core.Filters.ServiceOffers;
using Core.Models.ServiceOffers;
using System.Collections.Generic;
using System.Linq;

namespace Core.Repositories
{
    public interface IServiceOfferRepository : IRepository<ServiceOffer>
    {
        IQueryable<ServiceOffer> ApplyFilter(ServiceOfferListFilter filter);
        IQueryable<ServiceOffer> ApplySort(ServiceOfferListSortFilter sortFilter, IEnumerable<ServiceOffer> serviceOffers);
        IQueryable<ServiceOffer> ApplyPagination(ServiceOfferListPaginatedFilter paginationFilter, IEnumerable<ServiceOffer> serviceOffers);
    }
}
