﻿using Core.Enums;
using Core.Utils;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Core.Filters.Login
{
    public class UserLoginFilter
    {
        [Required]
        public string Email { get; set; }

        string _password;
        [Required]
        public string Password {
            get
            {
                return this._password;
            }
            set
            {
                this._password = EncryptUtil.ApplyHash(value);
            }
        }

        [Required]
        public UserProfilesEnum Profile { get; set; }
    }

}