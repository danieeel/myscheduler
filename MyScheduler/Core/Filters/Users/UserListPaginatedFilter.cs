﻿using Core.Models.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Filters.Users
{
    public static class UserListPaginatedFilterExtensions
    {
        public static IQueryable<User> ApplyPagination(this IQueryable<User> query, UserListPaginatedFilter filter)
        {
            if (filter != null)
            {
                return query
                       .Skip((filter.PageNumber - 1) * filter.PageSize)
                       .Take(filter.PageSize);
            }

            return query;
   
        }
    }
    public class UserListPaginatedFilter
    {
        private const int _defaultMaxPageSize = 200;
        private int _defaultPageSize = 50;

        public int PageNumber { get; set; } = 1;

        public int PageSize
        {
            get
            {
                return _defaultPageSize;
            }
            set
            {
                _defaultPageSize = (value > _defaultMaxPageSize) ? _defaultMaxPageSize : value;
            }
        }

    }
}
