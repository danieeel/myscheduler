﻿using Core.Enums;
using Core.Models.Users;
using System;
using System.Linq;
using System.Text.Json.Serialization;

namespace Core.Filters.Users
{
    public static class UserListFilterExtensions
    {
        public static IQueryable<User> ApplyFilter(this IQueryable<User> query, UserListFilter filter)
        {
            if (filter == null)
                return query;

            if (!string.IsNullOrWhiteSpace(filter.Name))
            {
                query = query.Where(user => user.Name.Contains(filter.Name, StringComparison.OrdinalIgnoreCase));
            }

            if (!string.IsNullOrWhiteSpace(filter.Email))
            {
                query = query.Where(user => user.Email.Contains(filter.Email, StringComparison.OrdinalIgnoreCase));
            }

            if (filter.Profile != null)
            {
                query = query.Where(user => user.Profile == filter.Profile);
            }

            if (filter.IsActive != null)
            {
                query = query.Where(user => user.IsActive == filter.IsActive);
            }

            return query;
        }

    }


    public class UserListFilter
    {
        public string Name { get; set; }
        public string Email { get; set; }

        [JsonConverter(typeof(JsonStringEnumConverter))]
        public UserProfilesEnum? Profile { get; set; }
        public bool? IsActive { get; set; }
    }

}