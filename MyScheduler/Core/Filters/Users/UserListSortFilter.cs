﻿using Core.Enums;
using Core.Models.Users;
using System;
using System.Linq;

namespace Core.Filters.Users
{
    public static class UserListSortFilterExtensions
    {
        public static IQueryable<User> ApplySort(this IQueryable<User> query, UserListSortFilter sortFilter)
        {
            if (sortFilter == null || string.IsNullOrWhiteSpace(sortFilter.OrderBy))
                return query.OrderBy(user => user.Name);

            if (sortFilter.OrderBy.Equals("NAME", StringComparison.OrdinalIgnoreCase))
            {
                if (sortFilter.OrderType == OrderType.DESC)
                {
                    return query.OrderByDescending(user => user.Name);
                }
                else
                {
                    return query.OrderBy(user => user.Name);
                }
            }

            if (sortFilter.OrderBy.Equals("EMAIL", StringComparison.OrdinalIgnoreCase))
            {
                if (sortFilter.OrderType == OrderType.DESC)
                {
                    return query.OrderByDescending(user => user.Email);
                }
                else
                {
                    return query.OrderBy(user => user.Email);
                }
            }

            if (sortFilter.OrderBy.Equals("PROFILE", StringComparison.OrdinalIgnoreCase))
            {
                if (sortFilter.OrderType == OrderType.DESC)
                {
                    return query.OrderByDescending(user => user.Profile);
                }
                else
                {
                    return query.OrderBy(user => user.Profile);
                }
            }

            return query.OrderBy(user => user.Name);
        }
    }

    public class UserListSortFilter
    {
        public string OrderBy { get; set; }

        public OrderType? OrderType { get; set; }

    }
}