﻿using Core.Models.ServiceOffers;
using System;
using System.Linq;

namespace Core.Filters.ServiceOffers
{
    public static class ServiceOfferListFilterExtensions
    {
        public static IQueryable<ServiceOffer> ApplyFilter(this IQueryable<ServiceOffer> query, ServiceOfferListFilter filter)
        {
            if (filter == null)
                return query;

            if (!string.IsNullOrWhiteSpace(filter.Name))
            {
                query = query.Where(serviceOffer => serviceOffer.Name.Contains(filter.Name, StringComparison.OrdinalIgnoreCase));
            }

            if (!string.IsNullOrWhiteSpace(filter.Description))
            {
                query = query.Where(serviceOffer => serviceOffer.Description.Contains(filter.Description, StringComparison.OrdinalIgnoreCase));
            }

            if (filter.IsActive != null)
            {
                query = query.Where(serviceOffer => serviceOffer.IsActive == filter.IsActive);
            }

            return query;
        }

    }


    public class ServiceOfferListFilter
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
    }

}