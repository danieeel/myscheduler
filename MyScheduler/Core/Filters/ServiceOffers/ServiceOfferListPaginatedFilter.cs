﻿using Core.Models.ServiceOffers;
using System.Linq;

namespace Core.Filters.ServiceOffers
{
    public static class ServiceOfferListPaginatedFilterExtensions
    {
        public static IQueryable<ServiceOffer> ApplyPagination(this IQueryable<ServiceOffer> query, ServiceOfferListPaginatedFilter filter)
        {
            if (filter != null)
            {
                return query
                       .Skip((filter.PageNumber - 1) * filter.PageSize)
                       .Take(filter.PageSize);
            }

            return query;

        }
    }
    public class ServiceOfferListPaginatedFilter
    {
        private const int _defaultMaxPageSize = 200;
        private int _defaultPageSize = 50;

        public int PageNumber { get; set; } = 1;

        public int PageSize
        {
            get
            {
                return _defaultPageSize;
            }
            set
            {
                _defaultPageSize = (value > _defaultMaxPageSize) ? _defaultMaxPageSize : value;
            }
        }

    }
}
