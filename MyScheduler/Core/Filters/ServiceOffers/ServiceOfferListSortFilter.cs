﻿
using Core.Enums;
using Core.Models.ServiceOffers;
using System;
using System.Linq;

namespace Core.Filters.ServiceOffers
{
    public static class ServiceOfferListSortFilterExtensions
    {
        public static IQueryable<ServiceOffer> ApplySort(this IQueryable<ServiceOffer> query, ServiceOfferListSortFilter sortFilter)
        {
            if (sortFilter == null || string.IsNullOrWhiteSpace(sortFilter.OrderBy))
                return query.OrderBy(serviceOffer => serviceOffer.Name);

            if (sortFilter.OrderBy.Equals("NAME", StringComparison.OrdinalIgnoreCase))
            {
                if (sortFilter.OrderType == OrderType.DESC)
                {
                    return query.OrderByDescending(serviceOffer => serviceOffer.Name);
                }
                else
                {
                    return query.OrderBy(serviceOffer => serviceOffer.Name);
                }
            }

            if (sortFilter.OrderBy.Equals("DESCRIPTION", StringComparison.OrdinalIgnoreCase))
            {
                if (sortFilter.OrderType == OrderType.DESC)
                {
                    return query.OrderByDescending(serviceOffer => serviceOffer.Description);
                }
                else
                {
                    return query.OrderBy(serviceOffer => serviceOffer.Description);
                }
            }

            return query.OrderBy(serviceOffer => serviceOffer.Name);
        }
    }

    public class ServiceOfferListSortFilter
    {
        public string OrderBy { get; set; }

        public OrderType? OrderType { get; set; }

    }
}