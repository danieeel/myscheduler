﻿using Core.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Models.Users
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public string Password { get; set; }

        [EnumDataType(typeof(UserProfilesEnum))]
        [JsonConverter(typeof(StringEnumConverter))]
        public UserProfilesEnum Profile { get; set; }

        public DateTime LastUpdate { get; set; }
        public DateTime CreatedAt { get; set; }

    }
}
