﻿using Core.Enums;
using System;

namespace Core.Models.Users.DTOs
{
    public class UserOutputDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }

        public String Profile { get; set; }
        public DateTime LastUpdate { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
