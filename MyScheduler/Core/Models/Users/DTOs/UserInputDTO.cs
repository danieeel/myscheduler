﻿using Core.Enums;
using Core.Utils;
using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Models.Users.DTOs
{
    public class UserInputDTO
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }

        string _password;
        [Required]
        public string Password
        {
            get
            {
                return this._password;
            }
            set
            {
                this._password = EncryptUtil.ApplyHash(value);
            }
        }
        public bool IsActive { get; set; }
        [Required]
        public UserProfilesEnum Profile { get; set; }

    }
}
