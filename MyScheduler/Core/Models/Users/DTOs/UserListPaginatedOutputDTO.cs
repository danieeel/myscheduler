﻿using Core.Filters;
using Core.Filters.Users;
using System;
using System.Collections.Generic;

namespace Core.Models.Users.DTOs
{

    public class UserListPaginatedOutputDTO
    {
        public int CurrentPage { get; private set; }
        public int TotalPages { get; private set; }
        public int PageSize { get; private set; }
        public int TotalCount { get; private set; }

        public bool HasPrevious => CurrentPage > 1;
        public bool HasNext => CurrentPage < TotalPages;

        public IEnumerable<UserOutputDTO> Items { get; private set; }

        public UserListPaginatedOutputDTO()
        {
            Items = new List<UserOutputDTO>();
        }

        public static UserListPaginatedOutputDTO BuildGenericListOutputDTO(IEnumerable<UserOutputDTO> items, UserListPaginatedFilter paginationFilter, int totalCount)
        {        
            return new UserListPaginatedOutputDTO()
            {
                TotalPages = (int)Math.Ceiling((double)totalCount / paginationFilter.PageSize),
                CurrentPage = paginationFilter.PageNumber,
                TotalCount = totalCount,
                PageSize = paginationFilter.PageSize,
                Items = items
            };
        }
    }
}
