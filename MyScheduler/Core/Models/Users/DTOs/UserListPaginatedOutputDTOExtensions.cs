﻿using Core.Filters;
using Core.Filters.Users;
using System.Collections.Generic;

namespace Core.Models.Users.DTOs
{
    public static class UserListPaginatedOutputDTOExtensions
    {
        public static UserListPaginatedOutputDTO BuildPaginatedResponse(this IEnumerable<UserOutputDTO> items, UserListPaginatedFilter paginationFilter, int totalCount)
        {
            return UserListPaginatedOutputDTO.BuildGenericListOutputDTO(items, paginationFilter, totalCount);
            
        }
    }

   
}
