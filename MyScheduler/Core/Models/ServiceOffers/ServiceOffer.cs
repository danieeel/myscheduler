﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.ServiceOffers
{
    public class ServiceOffer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public int? TimeNeeded { get; set; }

        public DateTime LastUpdate { get; set; }
        public DateTime CreatedAt { get; set; }

    }
}
