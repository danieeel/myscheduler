﻿using System;

namespace Core.Models.ServiceOffers.DTOs
{
    public class ServiceOfferOutputDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public DateTime LastUpdate { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
