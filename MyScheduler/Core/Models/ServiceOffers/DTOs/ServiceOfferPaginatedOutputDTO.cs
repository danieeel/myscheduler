﻿using Core.Filters.ServiceOffers;
using System;
using System.Collections.Generic;

namespace Core.Models.ServiceOffers.DTOs
{

    public class ServiceOfferListPaginatedOutputDTO
    {
        public int CurrentPage { get; private set; }
        public int TotalPages { get; private set; }
        public int PageSize { get; private set; }
        public int TotalCount { get; private set; }

        public bool HasPrevious => CurrentPage > 1;
        public bool HasNext => CurrentPage < TotalPages;

        public IEnumerable<ServiceOfferOutputDTO> Items { get; private set; }

        public ServiceOfferListPaginatedOutputDTO()
        {
            Items = new List<ServiceOfferOutputDTO>();
        }

        public static ServiceOfferListPaginatedOutputDTO BuildGenericListOutputDTO(IEnumerable<ServiceOfferOutputDTO> items, ServiceOfferListPaginatedFilter paginationFilter, int totalCount)
        {

            return new ServiceOfferListPaginatedOutputDTO()
            {
                TotalPages = (int)Math.Ceiling((double)totalCount / paginationFilter.PageSize),
                CurrentPage = paginationFilter.PageNumber,
                TotalCount = totalCount,
                PageSize = paginationFilter.PageSize,
                Items = items

            };
        }

    }
}
