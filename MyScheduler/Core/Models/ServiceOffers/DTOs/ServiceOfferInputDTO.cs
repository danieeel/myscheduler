﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Models.ServiceOffers.DTOs
{
    public class ServiceOfferInputDTO
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        public bool IsActive { get; set; }
      
        public DateTime LastUpdate { get; set; }
        public DateTime CreatedAt { get; set; }

    }
}
