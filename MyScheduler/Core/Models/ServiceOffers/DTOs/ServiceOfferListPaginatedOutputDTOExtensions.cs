﻿using Core.Filters.ServiceOffers;
using System.Collections.Generic;

namespace Core.Models.ServiceOffers.DTOs
{
    public static class ServiceOfferListPaginatedOutputDTOExtensions
    {
        public static ServiceOfferListPaginatedOutputDTO BuildPaginatedResponse(this IEnumerable<ServiceOfferOutputDTO> items, ServiceOfferListPaginatedFilter paginationFilter, int totalCount)
        {
            return ServiceOfferListPaginatedOutputDTO.BuildGenericListOutputDTO(items, paginationFilter, totalCount);
            
        }
    }

   
}
