﻿using Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository Users { get; }
        ILoginRepository Login { get; }

        IServiceOfferRepository ServiceOffers { get; }
        Task<int> CommitAsync();
    }
}
