﻿using System.ComponentModel;

namespace Core.Enums
{
    public enum UserProfilesEnum
    {
        [Description("Administrator")]
        Administrator = 1,

        [Description("Client")]
        Client = 2,

        [Description("Employee")]
        Employee = 3
    }
}
