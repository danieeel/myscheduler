﻿using System.ComponentModel;

namespace Core.Enums
{
    public enum OrderType
    {
        [Description("Ascending")]
        ASC = 1,
        [Description("Descending")]
        DESC = 2,
        
    }
}
