﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Enums
{
    public enum ActionTypesEnum
    {
        ADD,
        EDIT,
        DELETE
    }
}
