﻿using Core.Filters.ServiceOffers;
using Core.Models.ServiceOffers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Services
{
    public interface IServiceOfferService
    {
        Task<IEnumerable<ServiceOffer>> GetAll();

        IEnumerable<ServiceOffer> ApplyFilter(ServiceOfferListFilter filter);

        IEnumerable<ServiceOffer> ApplySort(ServiceOfferListSortFilter sortFilter, IEnumerable<ServiceOffer> serviceOffers);

        IEnumerable<ServiceOffer> ApplyPagination(ServiceOfferListPaginatedFilter paginationFilter, IEnumerable<ServiceOffer> serviceOffers);

        Task<ServiceOffer> GetById(int id);
        Task<ServiceOffer> Add(ServiceOffer serviceOffer);
        Task Update(ServiceOffer userToBeUpdated, ServiceOffer serviceOffer);
        Task Delete(ServiceOffer serviceOffer);
    }
}
