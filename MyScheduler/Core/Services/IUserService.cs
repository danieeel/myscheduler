﻿using Core.Filters.Users;
using Core.Models.Users;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Services
{
    public interface IUserService
    {
        Task<IEnumerable<User>> GetAll();

        IEnumerable<User> ApplyFilter(UserListFilter filter);

        IEnumerable<User> ApplySort(UserListSortFilter sortFilter, IEnumerable<User> users);

        IEnumerable<User> ApplyPagination(UserListPaginatedFilter paginationFilter, IEnumerable<User> users);

        Task<User> GetById(int id);
        Task<User> Add(User user);
        Task Update(User userToBeUpdated, User user);
        Task Delete(User user);
    }
}
