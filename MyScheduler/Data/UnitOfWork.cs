﻿using Core;
using Core.Repositories;
using Data.Entities;
using Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _userContext;
        
        private UserRepository _userRepository;
        private LoginRepository _loginRepository;
        private ServiceOfferRepository _serviceOfferRepository;


        public UnitOfWork(AppDbContext userContext)
        {
            this._userContext = userContext;
            
        }

        public IUserRepository Users => _userRepository ??= new UserRepository(_userContext);

        public ILoginRepository Login => _loginRepository ??= new LoginRepository(_userContext);

        public IServiceOfferRepository ServiceOffers => _serviceOfferRepository ??= new ServiceOfferRepository(_userContext);

        public async Task<int> CommitAsync()
        {
            if (this._userContext != null)
                return await _userContext.SaveChangesAsync().ConfigureAwait(true);
           

            return 0;
        }

        public void Dispose()
        {
            if (this._userContext != null)
                _userContext.Dispose();
           
        }
    }
}
