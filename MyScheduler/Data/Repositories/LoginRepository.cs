﻿using Core.Filters.Login;
using Core.Models.Users;
using Core.Repositories;
using Data.Entities;
using System;
using System.Linq;

namespace Data.Repositories
{
    public class LoginRepository : BaseRepository<User>, ILoginRepository
    {
        public LoginRepository(AppDbContext context)
            : base(context)
        { }

        public User Login(UserLoginFilter filter)
        {
            return Context.Set<User>()
                .Where(user => user.Email.ToLower().Equals(filter.Email.ToLower())
                            && user.Profile == filter.Profile
                            && user.Password == filter.Password)
                .FirstOrDefault();
        }
    }
}
