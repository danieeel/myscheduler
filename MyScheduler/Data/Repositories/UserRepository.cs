﻿using Core.Filters.Users;
using Core.Models.Users;
using Core.Repositories;
using Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Data.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(AppDbContext context)
            : base(context)
        { }


        private AppDbContext UserDbContext
        {
            get { return Context as AppDbContext; }
        }

        public IQueryable<User> ApplyFilter(UserListFilter filter)
        {
            return Context.Set<User>()
                          .ApplyFilter(filter);
        }

        public IQueryable<User> ApplySort(UserListSortFilter sortFilter, IEnumerable<User> users)
        {
            return users.AsQueryable<User>()
                .ApplySort(sortFilter);
        }

        public IQueryable<User> ApplyPagination(UserListPaginatedFilter paginationFilter, IEnumerable<User> users)
        {
            return users.AsQueryable<User>()
                .ApplyPagination(paginationFilter);
                
        }

        
    }
}
