﻿using Core.Filters.ServiceOffers;
using Core.Models.ServiceOffers;
using Core.Repositories;
using Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Data.Repositories
{
    public class ServiceOfferRepository : BaseRepository<ServiceOffer>, IServiceOfferRepository
    {
        public ServiceOfferRepository(AppDbContext context)
            : base(context)
        { }


        private AppDbContext ServiceOfferDbContext
        {
            get { return Context as AppDbContext; }
        }

        public IQueryable<ServiceOffer> ApplyFilter(ServiceOfferListFilter filter)
        {
            return Context.Set<ServiceOffer>()
                          .ApplyFilter(filter);
        }

        public IQueryable<ServiceOffer> ApplySort(ServiceOfferListSortFilter sortFilter, IEnumerable<ServiceOffer> serviceOffers)
        {
            return serviceOffers.AsQueryable<ServiceOffer>()
                .ApplySort(sortFilter);
        }

        public IQueryable<ServiceOffer> ApplyPagination(ServiceOfferListPaginatedFilter paginationFilter, IEnumerable<ServiceOffer> serviceOffers)
        {
            return serviceOffers.AsQueryable<ServiceOffer>()
                .ApplyPagination(paginationFilter);

        }


    }
}
