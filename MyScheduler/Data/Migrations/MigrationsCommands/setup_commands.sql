﻿"INSERT INTO Users (Name, Email, IsActive, CreatedAt, LastUpdate) Values ('User 1', 'user1@myemail.com', 1, '2018-06-23 07:30:20', '2018-06-23 07:30:20' )"


dotnet ef --startup-project MySchedulerAPI/MySchedulerAPI.csproj migrations add InitialModel  -p Data/Data.csproj
dotnet ef --startup-project MySchedulerAPI/MySchedulerAPI.csproj migrations add InitialModel  -p Data/Data.csproj

dotnet ef --startup-project MySchedulerAPI/MySchedulerAPI.csproj database update 

dotnet ef --startup-project MySchedulerAPI/MySchedulerAPI.csproj migrations add SeedUsersTable -p Data/Data.csproj
dotnet ef --startup-project MySchedulerAPI/MySchedulerAPI.csproj migrations add SeedServiceOffersTable -p Data/Data.csproj

migrationBuilder
            .Sql($"INSERT INTO Users (Name, Email, IsActive, CreatedAt, LastUpdate) Values ('User 1', 'user1@myemail.com', 1, 1, '2018-06-23 07:30:20', '2018-06-23 07:30:20' )");

            migrationBuilder
              .Sql($"INSERT INTO Users (Name, Email, IsActive, CreatedAt, LastUpdate) Values ('User 2', 'user2@myemail.com', 1, 1, '2018-06-23 07:30:20', '2018-06-23 07:30:20' )");

            migrationBuilder
             .Sql($"INSERT INTO Users (Name, Email, IsActive, CreatedAt, LastUpdate) Values ('User 3', 'user3@myemail.com', 1, 1, '2018-06-23 07:30:20', '2018-06-23 07:30:20' )");

            migrationBuilder
             .Sql($"INSERT INTO Users (Name, Email, IsActive, CreatedAt, LastUpdate) Values ('User 4', 'user4@myemail.com', 0, 2, '2018-06-23 07:30:20', '2018-06-23 07:30:20' )");
