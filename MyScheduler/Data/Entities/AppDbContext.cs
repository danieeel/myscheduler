﻿using Core.Models.ServiceOffers;
using Core.Models.Users;
using Data.Configurations;
using Microsoft.EntityFrameworkCore;

namespace Data.Entities
{
    public class AppDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<ServiceOffer> ServiceOffers { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder
                .ApplyConfiguration(new UserConfiguration())
                .ApplyConfiguration(new ServiceOfferConfiguration());

        }
    }
}
