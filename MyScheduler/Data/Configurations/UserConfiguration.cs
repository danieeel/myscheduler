﻿using Core.Models.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder
                .HasKey(m => m.Id);

            builder
                .Property(m => m.Id)
                .UseIdentityColumn();

            builder
                .Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(50);

            builder
                .Property(m => m.Email)
                .IsRequired()
                .HasMaxLength(200);

            builder
                .Property(m => m.Password)
                .IsRequired()
                .HasMaxLength(200);

            builder
                .Property(m => m.IsActive)
                .IsRequired();

            builder
                .Property(m => m.Profile)
                
                .IsRequired();
            
            builder
                .Property(m => m.LastUpdate);

            builder
                .Property(m => m.CreatedAt);
               
            builder
                .ToTable("Users");
        }
    }
}
