﻿using Core.Models.ServiceOffers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.Configurations
{
    public class ServiceOfferConfiguration : IEntityTypeConfiguration<ServiceOffer>
    {
        public void Configure(EntityTypeBuilder<ServiceOffer> builder)
        {
            builder
                .HasKey(m => m.Id);

            builder
                .Property(m => m.Id)
                .UseIdentityColumn();

            builder
                .Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(50);

            builder
                .Property(m => m.Description)
                .IsRequired()
                .HasMaxLength(1000);

            builder
                .Property(m => m.IsActive)
                .IsRequired();

            builder
                .Property(m => m.LastUpdate);

            builder
                .Property(m => m.CreatedAt);
               
            builder
                .ToTable("ServiceOffers");
        }
    }
}
